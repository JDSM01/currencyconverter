package com.example.currencyconverter

import android.app.Application
import com.example.currencyconverter.dagger.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MyApplication : Application(), HasAndroidInjector {

  @Inject
  lateinit var androidInjector: DispatchingAndroidInjector<Any>

  override fun onCreate() {
    super.onCreate()
    val appComponent = DaggerAppComponent.builder()
        .application(this)
        .build()
    appComponent.inject(this)
  }

  override fun androidInjector() = androidInjector
}