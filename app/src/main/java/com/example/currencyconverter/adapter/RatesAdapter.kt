package com.example.currencyconverter.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.R
import com.example.currencyconverter.helpers.ItemMapper
import com.example.currencyconverter.listener.CurrencyTextWatcher
import com.example.currencyconverter.models.CurrencyValueItem
import com.example.currencyconverter.view.RatesViewHolder
import io.reactivex.subjects.PublishSubject
import java.util.*


class RatesAdapter(
    private val itemClickSubject: PublishSubject<Int>,
    private val textChangedSubject: PublishSubject<String>,
    ratesList: MutableList<CurrencyValueItem> = Collections.emptyList()
) :
    RecyclerView.Adapter<RatesViewHolder>() {

  private var currentRateList = ratesList

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
    val layout = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_rate, parent, false)
    return RatesViewHolder(layout, ItemMapper(), itemClickSubject,
        CurrencyTextWatcher(textChangedSubject))
  }

  override fun getItemCount() = currentRateList.size

  override fun onBindViewHolder(
      holder: RatesViewHolder, position: Int,
      payloads: MutableList<Any>
  ) {
    val rateItem = currentRateList[position]
    when {
      payloads.isEmpty() -> holder.bind(rateItem.currency, rateItem.value)
      payloads[0] is String && payloads[0] == "refresh" -> holder.updateValue(rateItem.value)
      payloads[0] is Boolean -> {
        if (payloads[0] as Boolean) holder.requestFocus(rateItem.currency)
        else holder.removeFocus()
      }
    }
  }

  fun refreshRates(newValuesList: List<CurrencyValueItem>) {
    if (currentRateList.size == 1) {
      currentRateList = newValuesList.toMutableList()
      notifyDataSetChanged()
    } else {
      newValuesList.forEach { item ->
        val index = currentRateList.indexOfFirst { it.currency == item.currency }
        if (index != -1) {
          currentRateList[index] = item
          if (index != 0) notifyItemChanged(index, "refresh")
        } else {
          currentRateList.add(item)
        }
      }
    }
  }

  fun moveToFirstPosition(position: Int) {
    val item = currentRateList[position]
    currentRateList.removeAt(position)
    currentRateList.add(0, item)
    notifyItemMoved(position, 0)
    notifyItemChanged(0, true)
    notifyItemChanged(1, false)
  }

  override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {
  }

  override fun onViewDetachedFromWindow(holder: RatesViewHolder) {
    if (holder.adapterPosition == 0) holder.closeKeyboard()
  }
}
