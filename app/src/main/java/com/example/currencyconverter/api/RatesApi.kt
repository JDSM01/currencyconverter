package com.example.currencyconverter.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

  @GET("latest")
  fun getRates(@Query("base") baseCurrency: String): Single<RatesResponse>

}
