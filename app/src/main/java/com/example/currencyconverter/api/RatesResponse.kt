package com.example.currencyconverter.api

import com.google.gson.JsonObject

data class RatesResponse(val baseCurrency: String, val rates: JsonObject)
