package com.example.currencyconverter.dagger

import com.example.currencyconverter.view.RatesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilders {

  @ActivityScope
  @ContributesAndroidInjector
  internal abstract fun bindRatesActivity(): RatesActivity
}
