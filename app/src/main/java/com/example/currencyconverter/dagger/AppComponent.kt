package com.example.currencyconverter.dagger

import com.example.currencyconverter.MyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
      AppModule::class,
      ActivityBuilders::class,
      FragmentBuilders::class]
)
interface AppComponent {

  fun inject(app: MyApplication?)

  @Component.Builder
  interface Builder {
    @BindsInstance
    fun application(app: MyApplication): Builder
    fun build(): AppComponent
  }
}