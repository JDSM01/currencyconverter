package com.example.currencyconverter.dagger

import com.example.currencyconverter.*
import com.example.currencyconverter.api.RatesApi
import com.example.currencyconverter.helpers.LogInterceptor
import com.example.currencyconverter.interactor.RatesInteractor
import com.example.currencyconverter.repository.RatesRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
internal class AppModule {

  @Provides
  fun providesRatesInteractor(ratesRepository: RatesRepository): RatesInteractor {
    return RatesInteractor(ratesRepository)
  }

  @Provides
  fun providesRatesRepository(ratesApi: RatesApi): RatesRepository {
    return RatesRepository(ratesApi)
  }

  @Provides
  fun providesOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(LogInterceptor())
        .connectTimeout(15, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.MINUTES)
        .writeTimeout(30, TimeUnit.MINUTES)
        .build()
  }

  @Provides
  fun providesRatesApi(okHttpClient: OkHttpClient): RatesApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.RATES_HOST)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(RatesApi::class.java)
  }
}
