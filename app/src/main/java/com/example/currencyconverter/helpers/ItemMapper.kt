package com.example.currencyconverter.helpers

import android.content.Context
import android.graphics.drawable.Drawable
import com.example.currencyconverter.models.CurrencyValueItem
import com.jwang123.flagkit.FlagKit
import java.math.BigDecimal
import java.util.*

class ItemMapper {

  fun mapFlagAndFullName(context: Context, name: String): Pair<Drawable?, String> {
    return when (name) {
      "AUD" -> Pair(getFlagDrawable(context, "au"), "Austrailia Dollar")
      "BGN" -> Pair(getFlagDrawable(context, "bg"), "Bulgarian Iev")
      "BRL" -> Pair(getFlagDrawable(context, "br"), "Brazil Real")
      "CAD" -> Pair(getFlagDrawable(context, "ca"), "Canadian Dollar")
      "CHF" -> Pair(getFlagDrawable(context, "ch"), "Switzerland Franc")
      "CNY" -> Pair(getFlagDrawable(context, "cn"), "China Yuan/Renminbi")
      "CZK" -> Pair(getFlagDrawable(context, "cz"), "Czeck Koruna")
      "DKK" -> Pair(getFlagDrawable(context, "dk"), "Denmark Krone")
      "EUR" -> Pair(getFlagDrawable(context, "pt"), "Euro")
      "GBP" -> Pair(getFlagDrawable(context, "gb"), "Great Britain Pound")
      "HKD" -> Pair(getFlagDrawable(context, "hk"), "Hong Kong Dollar")
      "HRK" -> Pair(getFlagDrawable(context, "hr"), "Croatia Kuna")
      "HUF" -> Pair(getFlagDrawable(context, "hu"), "Hungary Forint")
      "IDR" -> Pair(getFlagDrawable(context, "id"), "Indonesia Rupiah")
      "ILS" -> Pair(getFlagDrawable(context, "il"), "Israel New Shekel")
      "INR" -> Pair(getFlagDrawable(context, "in"), "India Rupee")
      "ISK" -> Pair(getFlagDrawable(context, "is"), "Iceland Krona")
      "JPY" -> Pair(getFlagDrawable(context, "jp"), "Japan Yen")
      "KRW" -> Pair(getFlagDrawable(context, "kr"), "South Korea Won")
      "MXN" -> Pair(getFlagDrawable(context, "mx"), "Mexico Peso")
      "MYR" -> Pair(getFlagDrawable(context, "my"), "Malasya Ringgit")
      "NOK" -> Pair(getFlagDrawable(context, "no"), "Norway Kroner")
      "NZD" -> Pair(getFlagDrawable(context, "nz"), "New Zealand Dollar")
      "PHP" -> Pair(getFlagDrawable(context, "ph"), "Philippines Peso")
      "PLN" -> Pair(getFlagDrawable(context, "pl"), "Poland Zloty")
      "RON" -> Pair(getFlagDrawable(context, "ro"), "Romania New Lei")
      "RUB" -> Pair(getFlagDrawable(context, "ru"), "Russia Rouble")
      "SEK" -> Pair(getFlagDrawable(context, "se"), "Sweden Krona")
      "SGD" -> Pair(getFlagDrawable(context, "sg"), "Singapore Dollar")
      "THB" -> Pair(getFlagDrawable(context, "th"), "Thailand Baht")
      "USD" -> Pair(getFlagDrawable(context, "us"), "USA Dollar")
      "ZAR" -> Pair(getFlagDrawable(context, "za"), "South Africa Rand")
      else -> Pair(null, "")
    }
  }

  fun getDefaultItem(countryCode: String): CurrencyValueItem {
    return when (countryCode.toLowerCase(Locale.ROOT)) {
      "au" -> CurrencyValueItem("AUD", BigDecimal(100))
      "bg" -> CurrencyValueItem("BGN", BigDecimal(100))
      "br" -> CurrencyValueItem("BRL", BigDecimal(100))
      "ca" -> CurrencyValueItem("CAD", BigDecimal(100))
      "ch" -> CurrencyValueItem("CHF", BigDecimal(100))
      "cn" -> CurrencyValueItem("CNY", BigDecimal(100))
      "cz" -> CurrencyValueItem("CZK", BigDecimal(100))
      "dk" -> CurrencyValueItem("DKK", BigDecimal(100))
      "gb" -> CurrencyValueItem("GBP", BigDecimal(100))
      "hk" -> CurrencyValueItem("HKD", BigDecimal(100))
      "hr" -> CurrencyValueItem("HRK", BigDecimal(100))
      "hu" -> CurrencyValueItem("HUF", BigDecimal(100))
      "id" -> CurrencyValueItem("IDR", BigDecimal(100))
      "il" -> CurrencyValueItem("ILS", BigDecimal(100))
      "in" -> CurrencyValueItem("INR", BigDecimal(100))
      "is" -> CurrencyValueItem("ISK", BigDecimal(100))
      "jp" -> CurrencyValueItem("JPY", BigDecimal(100))
      "kp" -> CurrencyValueItem("KWR", BigDecimal(100))
      "mx" -> CurrencyValueItem("MXN", BigDecimal(100))
      "my" -> CurrencyValueItem("MYR", BigDecimal(100))
      "no" -> CurrencyValueItem("NOK", BigDecimal(100))
      "nz" -> CurrencyValueItem("NZD", BigDecimal(100))
      "ph" -> CurrencyValueItem("PHP", BigDecimal(100))
      "pl" -> CurrencyValueItem("PLN", BigDecimal(100))
      "ro" -> CurrencyValueItem("RON", BigDecimal(100))
      "ru" -> CurrencyValueItem("RUB", BigDecimal(100))
      "se" -> CurrencyValueItem("SEK", BigDecimal(100))
      "sg" -> CurrencyValueItem("SGD", BigDecimal(100))
      "th" -> CurrencyValueItem("THB", BigDecimal(100))
      "us" -> CurrencyValueItem("USD", BigDecimal(100))
      "za" -> CurrencyValueItem("ZAR", BigDecimal(100))
      else -> CurrencyValueItem("EUR", BigDecimal(100))
    }
  }

  private fun getFlagDrawable(context: Context, countryCode: String): Drawable {
    return FlagKit.drawableWithFlag(context, countryCode)
  }
}
