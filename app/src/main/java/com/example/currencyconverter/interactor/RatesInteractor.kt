package com.example.currencyconverter.interactor

import com.example.currencyconverter.models.CurrencyValueItem
import com.example.currencyconverter.models.RatesModel
import com.example.currencyconverter.repository.RatesRepository
import io.reactivex.Single
import java.math.BigDecimal
import java.math.RoundingMode

class RatesInteractor(private val ratesRepository: RatesRepository) {


  fun retrieveConvertedValues(baseCurrency: String,
                              baseValue: BigDecimal,
                              useCache: Boolean = false): Single<List<CurrencyValueItem>> {
    return ratesRepository.getRates(baseCurrency, useCache)
        .map { convertRates(it, baseCurrency, baseValue) }
  }

  private fun convertRates(ratesModel: RatesModel,
                           baseCurrency: String,
                           baseValue: BigDecimal): List<CurrencyValueItem> {
    val currencyValuesList = ArrayList<CurrencyValueItem>()
    currencyValuesList.add(CurrencyValueItem(baseCurrency, baseValue))
    ratesModel.ratesList.forEach {
      val value = it.rate.multiply(baseValue)
          .setScale(2, RoundingMode.HALF_EVEN)
      currencyValuesList.add(CurrencyValueItem(it.currencyCode, value))
    }
    return currencyValuesList
  }

}
