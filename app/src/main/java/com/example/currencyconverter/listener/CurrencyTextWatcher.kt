package com.example.currencyconverter.listener

import android.text.Editable
import android.text.TextWatcher
import io.reactivex.subjects.PublishSubject

class CurrencyTextWatcher(private val textChangedSubject: PublishSubject<String>) :
    TextWatcher {

  private var currency: String? = null

  override fun afterTextChanged(s: Editable?) = Unit

  override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    currency?.let { textChangedSubject.onNext(it) }
  }

  fun setCurrency(currency: String) {
    this.currency = currency
  }
}