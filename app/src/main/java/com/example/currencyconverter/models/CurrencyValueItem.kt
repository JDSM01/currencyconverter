package com.example.currencyconverter.models

import java.math.BigDecimal

data class CurrencyValueItem(val currency: String, val value: BigDecimal)
