package com.example.currencyconverter.models

import java.math.BigDecimal

data class RateItem(val currencyCode: String, val rate: BigDecimal)