package com.example.currencyconverter.models

import com.example.currencyconverter.models.RateItem

data class RatesModel(val baseCurrency: String, val ratesList: List<RateItem>)
