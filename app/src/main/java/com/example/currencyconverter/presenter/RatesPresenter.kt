package com.example.currencyconverter.presenter

import com.example.currencyconverter.interactor.RatesInteractor
import com.example.currencyconverter.view.RatesView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class RatesPresenter(
    private val view: RatesView,
    private val interactor: RatesInteractor,
    private val disposables: CompositeDisposable,
    private val viewScheduler: Scheduler,
    private val networkScheduler: Scheduler
) {

  fun present() {
    handleRefreshRates()
    handleItemClick()
    handleValueChanged()
  }

  private fun handleValueChanged() {
    disposables.add(view.getValueChanged()
        .subscribeOn(networkScheduler)
        .flatMapMaybe { currency ->
          view.getBaseCurrencyAndValue()
              .filter { it.currency == currency }
        }
        .flatMapSingle { interactor.retrieveConvertedValues(it.currency, it.value, true) }
        .observeOn(viewScheduler)
        .doOnNext { view.updateList(it) }
        .doOnError { it.printStackTrace() }
        .retry()
        .subscribe({}, { it.printStackTrace() })
    )
  }

  private fun handleRefreshRates() {
    disposables.add(Observable.interval(1, TimeUnit.SECONDS)
        .subscribeOn(networkScheduler)
        .flatMapSingle { view.getBaseCurrencyAndValue() }
        .flatMapSingle { interactor.retrieveConvertedValues(it.currency, it.value) }
        .observeOn(viewScheduler)
        .doOnNext { view.updateList(it) }
        .doOnError { it.printStackTrace() }
        .retry()
        .subscribe({}, { it.printStackTrace() })
    )
  }

  private fun handleItemClick() {
    disposables.add(view.getItemClick()
        .throttleFirst(250, TimeUnit.MILLISECONDS)
        .filter { it != 0 }
        .observeOn(viewScheduler)
        .doOnNext { view.moveItemToFirstPosition(it) }
        .subscribe({}, { it.printStackTrace() })
    )
  }

  fun stop() = disposables.clear()
}
