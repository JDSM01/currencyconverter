package com.example.currencyconverter.repository

import com.example.currencyconverter.models.RateItem
import com.example.currencyconverter.api.RatesApi
import com.example.currencyconverter.models.RatesModel
import com.example.currencyconverter.api.RatesResponse
import io.reactivex.Single

class RatesRepository(private val api: RatesApi, private var cachedRatesModel: RatesModel? = null) {


  fun getRates(baseCurrency: String, useCache: Boolean = false): Single<RatesModel> {
    return if (useCache && cachedRatesModel != null && cachedRatesModel!!.baseCurrency == baseCurrency) {
      Single.just(cachedRatesModel)
    } else {
      api.getRates(baseCurrency)
          .map { map(it) }
          .doOnSuccess { cachedRatesModel = it }
    }
  }

  private fun map(ratesResponse: RatesResponse): RatesModel {
    val ratesList = ArrayList<RateItem>()
    ratesResponse.rates.entrySet()
        .forEach { ratesList.add(RateItem(it.key, it.value.asBigDecimal)) }
    return RatesModel(ratesResponse.baseCurrency, ratesList)
  }
}
