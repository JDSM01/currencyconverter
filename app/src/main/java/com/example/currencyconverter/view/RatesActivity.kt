package com.example.currencyconverter.view

import android.content.Context
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.currencyconverter.R
import com.example.currencyconverter.adapter.RatesAdapter
import com.example.currencyconverter.helpers.ItemMapper
import com.example.currencyconverter.interactor.RatesInteractor
import com.example.currencyconverter.models.CurrencyValueItem
import com.example.currencyconverter.presenter.RatesPresenter
import dagger.android.AndroidInjection
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_rates_layout.*
import kotlinx.android.synthetic.main.item_rate.view.*
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class RatesActivity : AppCompatActivity(), RatesView {

  @Inject
  lateinit var interactor: RatesInteractor
  private lateinit var presenter: RatesPresenter
  private lateinit var adapter: RatesAdapter
  private var itemClickSubject: PublishSubject<Int>? = null
  private var textChangedSubject: PublishSubject<String>? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    AndroidInjection.inject(this)
    setContentView(R.layout.activity_rates_layout)
    itemClickSubject = PublishSubject.create()
    textChangedSubject = PublishSubject.create()
    val itemMapper = ItemMapper()
    val initialItem = itemMapper.getDefaultItem(getUserCountryCode())
    adapter =
        RatesAdapter(itemClickSubject!!, textChangedSubject!!, listOf(initialItem) as MutableList)
    rates_recycler.adapter = adapter
    presenter =
        RatesPresenter(this, interactor, CompositeDisposable(), AndroidSchedulers.mainThread(),
            Schedulers.io())
    presenter.present()
  }

  override fun onDestroy() {
    itemClickSubject = null
    textChangedSubject = null
    presenter.stop()
    super.onDestroy()
  }

  override fun getItemClick(): Observable<Int> {
    return itemClickSubject!!
  }

  override fun moveItemToFirstPosition(position: Int) {
    adapter.moveToFirstPosition(position)
    rates_recycler.scrollToPosition(0)
  }

  override fun getBaseCurrencyAndValue(): Single<CurrencyValueItem> {
    val item = rates_recycler.findViewHolderForAdapterPosition(0)
    return item?.let {
      val value = item.itemView.value.text.toString()
      Single.just(CurrencyValueItem(item.itemView.currency_name.text.toString(), BigDecimal(value)))
    } ?: Single.error(Throwable("Unable to retrieve value"))
  }

  override fun updateList(currencyValueItemList: List<CurrencyValueItem>) {
    loading_spin.visibility = View.GONE
    adapter.refreshRates(currencyValueItemList)
  }

  override fun getValueChanged(): Observable<String> {
    return textChangedSubject!!
  }

  private fun getUserCountryCode(): String {
    val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    var userCountry = Locale.getDefault()
        .country
    val simCountry = telephonyManager.simCountryIso
    if (hasCorrectCountryFormat(simCountry)) {
      userCountry = simCountry
    } else if (isPhoneTypeReliable(telephonyManager)) { // device is not 3G (would be unreliable)
      val networkCountry = telephonyManager.networkCountryIso
      if (hasCorrectCountryFormat(networkCountry)) {
        userCountry = networkCountry
      }
    }
    return userCountry
  }

  private fun hasCorrectCountryFormat(country: String?): Boolean {
    return country != null && country.length == 2
  }

  private fun isPhoneTypeReliable(telephonyManager: TelephonyManager): Boolean {
    return telephonyManager.phoneType != TelephonyManager.PHONE_TYPE_CDMA
  }
}