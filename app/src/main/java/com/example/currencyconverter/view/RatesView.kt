package com.example.currencyconverter.view

import com.example.currencyconverter.models.CurrencyValueItem
import io.reactivex.Observable
import io.reactivex.Single

interface RatesView {
  fun getItemClick(): Observable<Int>

  fun moveItemToFirstPosition(position: Int)

  fun getBaseCurrencyAndValue(): Single<CurrencyValueItem>

  fun updateList(currencyValueItemList: List<CurrencyValueItem>)

  fun getValueChanged(): Observable<String>

}
