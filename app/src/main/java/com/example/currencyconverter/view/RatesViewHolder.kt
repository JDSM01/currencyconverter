package com.example.currencyconverter.view

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.currencyconverter.helpers.ItemMapper
import com.example.currencyconverter.listener.CurrencyTextWatcher
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_rate.view.*
import java.math.BigDecimal
import java.util.*


class RatesViewHolder(
    itemView: View,
    private val itemMapper: ItemMapper,
    private val itemClickSubject: PublishSubject<Int>,
    private val textWatcher: CurrencyTextWatcher
) :
    RecyclerView.ViewHolder(itemView) {

  fun bind(currency: String, rate: BigDecimal) {
    val flagAndName = itemMapper.mapFlagAndFullName(itemView.context, currency)
    itemView.currency_name.text = currency
    itemView.value.setText(String.format(rate.toString(), Locale.ROOT))
    itemView.setOnClickListener { itemClickSubject.onNext(adapterPosition) }
    if (adapterPosition != 0) {
      itemView.value.removeTextChangedListener(textWatcher)
      removeFocus()
    } else {
      addInitialSetup(currency)
    }
    itemView.currency_full_name.text = flagAndName.second
    flagAndName.first?.let {
      Glide.with(itemView.context)
          .load(it)
          .circleCrop()
          .into(itemView.flag)
    }
  }

  private fun addInitialSetup(currency: String) {
    itemView.value.isFocusableInTouchMode = true
    itemView.value.setOnClickListener(null)
    textWatcher.setCurrency(currency)
    itemView.value.removeTextChangedListener(textWatcher)
    itemView.value.addTextChangedListener(textWatcher)
  }

  fun requestFocus(currency: String) {
    addInitialSetup(currency)
    itemView.value.requestFocus()
    val imm: InputMethodManager =
        itemView.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(itemView.value, 0)
  }

  fun updateValue(value: BigDecimal) {
    itemView.value.setText(String.format(value.toString(), Locale.ROOT))
  }

  fun removeFocus() {
    itemView.value.removeTextChangedListener(textWatcher)
    itemView.value.isFocusableInTouchMode = false
    itemView.value.setOnClickListener { itemClickSubject.onNext(adapterPosition) }
  }

  fun closeKeyboard() {
    val imm: InputMethodManager =
        itemView.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(itemView.windowToken, 0)
  }
}
