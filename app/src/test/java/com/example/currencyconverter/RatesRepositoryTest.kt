package com.example.currencyconverter

import com.example.currencyconverter.api.RatesApi
import com.example.currencyconverter.api.RatesResponse
import com.example.currencyconverter.models.RateItem
import com.example.currencyconverter.models.RatesModel
import com.example.currencyconverter.repository.RatesRepository
import com.google.gson.JsonObject
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal

@RunWith(MockitoJUnitRunner::class)
class RatesRepositoryTest {

  @Mock
  lateinit var ratesApi: RatesApi
  private lateinit var ratesResponse: RatesResponse
  private val baseCurrency = "EUR"
  private val responseCurrency = "USD"
  private val responseRate = BigDecimal.valueOf(1.10)

  @Before
  fun setup() {
    val jsonObject = JsonObject()
    jsonObject.addProperty(responseCurrency, responseRate)
    ratesResponse = RatesResponse(baseCurrency, jsonObject)
    Mockito.`when`(ratesApi.getRates(baseCurrency))
        .thenReturn(Single.just(ratesResponse))
  }

  @Test
  fun nullCacheTest() {
    val observer = TestObserver<RatesModel>()
    val repository = RatesRepository(ratesApi)
    repository.getRates(baseCurrency, true)
        .subscribe(observer)
    observer.assertNoErrors()
  }

  @Test
  fun differentBaseCurrencyCacheTest() {
    val observer = TestObserver<RatesModel>()
    val cachedRatesModel = RatesModel("USD", emptyList())
    val repository = RatesRepository(ratesApi, cachedRatesModel)
    repository.getRates(baseCurrency, true)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue { it != cachedRatesModel }
  }

  @Test
  fun cacheUseTest() {
    val observer = TestObserver<RatesModel>()
    val cachedRatesModel = RatesModel("EUR", emptyList())
    val repository = RatesRepository(ratesApi, cachedRatesModel)
    repository.getRates(baseCurrency, true)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue { it == cachedRatesModel }
  }

  @Test
  fun ratesRequestTest() {
    val observer = TestObserver<RatesModel>()
    val ratesModel = RatesModel(baseCurrency, listOf(RateItem(responseCurrency, responseRate)))
    val repository = RatesRepository(ratesApi)
    repository.getRates(baseCurrency)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue { it == ratesModel }
  }

}